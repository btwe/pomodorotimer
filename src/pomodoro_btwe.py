# Copyright (c) 2022 btwe
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
"""Pomodoro Timer and Simple Alerts

A small and helpful widget for the QTile Bar


https://gitlab.com/btwe/pomodorotimer
"""

import asyncio
import subprocess
import enum
import shlex
import logging

from libqtile.utils import send_notification
from libqtile.widget import base
from libqtile.popup import Popup
from libqtile.log_utils import logger
from libqtile.widget.prompt import AbstractCompleter
from libqtile.command.base import expose_command

from datetime import datetime, timedelta
from functools import partial
from typing import Callable


# general convert functions:
def convert_seconds_to_ui_duration(secs: int) -> str:
    """
    input is `secs: int`
    returns str formatted in `HH:MM:SS`
    """
    fields = [int(secs / (60 * 60)), 0, 0]
    secs %= 60 * 60
    fields[1] = int(secs / (60))
    secs %= 60
    fields[2] = int(secs)
    return ":".join(f"{field:02d}" for field in fields)


def convert_ui_duration_to_seconds(uitime: str) -> float:
    match uitime.split(":"):
        case [hh, mm, ss]:
            secs = 60 * 60 * float(hh) + 60 * float(mm) + float(ss)
        case [mm, ss]:
            secs = 60 * float(mm) + float(ss)
        case [ss]:
            secs = float(ss)
        case _:
            raise Exception(f'could not convert "{uitime}" to seconds')
    return secs


def are_in_interval(v: list[str], left: int = 0, right: int = 59) -> bool:
    """
     v: list of str (which should contain integers)
     to be checked if all int values are within the interval (inklusive) given
     by left and right

    returns true/false
    """
    return all([int(e) >= left and int(e) <= right for e in v])


def convert_ui_attime_to_duration(attime: str) -> int:
    now = datetime.now()
    match attime.split(":"):
        case [hh, mm] if are_in_interval([hh, mm]):
            target = now.replace(hour=int(hh), minute=int(mm), second=0)
        case [hh, mm, ss] if are_in_interval([hh, mm, ss]):
            target = now.replace(hour=int(hh), minute=int(mm), second=int(ss))
        case _:
            raise Exception(f'could not parse "{attime}" as a time.')
    delta = target - now
    if delta.days == -1 or delta.days == 0:
        secs = delta.seconds
    return secs


class PomodoroBase(base._TextBox):
    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
        ("font", "Noto Sans Mono", ""),
        ("notifications", True, "En/Disable notifications"),
        ("logfile", None, "Path to log files or None"),
        ("logformat", "%(asctime)s %(name)s %(levelname)s %(message)s", ""),
        ("loglevel", "INFO", "loglevel name for the logfile"),
    ]

    def __init__(
        self,
        pomname=__name__,
        **config,
    ):
        super().__init__(**config)
        self.add_defaults(PomodoroBase.defaults)
        self.setup_mouse_callbacks()
        self.can_update = False
        self._timerlogger = logging.getLogger(pomname)
        try:
            llv = logging._nameToLevel[self.loglevel]
        except KeyError:
            llv = logging.INFO
        self._timerlogger.setLevel(llv)
        self._timerlogger.propagate = True
        if self.logfile:
            fh = logging.FileHandler(self.logfile)
            fmtr = logging.Formatter(self.logformat)
            fh.setFormatter(fmtr)
            self._timerlogger.addHandler(fh)
        self._timerlogger.debug("Starting log")

    @property
    def log(self):
        return self._timerlogger

    def timer_setup(self):
        self.can_update = True
        self.update()

    def setup_mouse_callbacks(self):
        mouse_cb = {
            "Button1": self._button1,
            "Button2": self._button2,
            "Button3": self._button3,
            "Button4": self._button4,
            "Button5": self._button5,
        }
        self.add_callbacks(mouse_cb)

    def _send_notification(
        self,
        message: str,
        urgent: bool = False,
    ) -> None:
        if self.notifications:
            send_notification(
                title=self.notification_title,
                message=message,
                urgent=urgent,
                timeout=0 if urgent else 10000,
            )

    def update(self):
        if self.can_update:
            super().update(self.render_output())

    def _button1(self):
        pass

    def _button2(self):
        pass

    def _button3(self):
        pass

    def _button4(self):
        """scroll up"""
        pass

    def _button5(self):
        """scroll down"""
        pass

    def mouse_enter(self, x, y):
        self.update()

    def mouse_leave(self, x, y):
        self.update()


class PomodoroTimer(PomodoroBase):
    """# Pomodoro technique widget
    see https://en.wikipedia.org/wiki/Pomodoro_Technique

    A goal of the technique is to reduce the effect of internal and
    external interruptions on focus and flow.

    ## Description
    The original technique has six steps:

    1. Decide on the task to be done.
    2. Set the pomodoro timer (typically for 25 minutes).[1]
    3. Work on the task.
    4. End work when the timer rings and take a short break (typically 5–10 minutes).[5]
    5. If you have finished fewer than three pomodoros, go back to Step 2
       and repeat until you go through all three pomodoros.
    6. After three pomodoros are done, take the fourth pomodoro and then
       take a long break (typically 20 to 30 minutes). Once the long break is
       finished, return to step 2.

    After task completion in a pomodoro, any remaining time should be
    devoted to activities, for example:

    1. Review your work just completed.
    2. Review the activities from a learning point of view (ex: What
       learning objective did you accomplish? What learning outcome did you
       accomplish? Did you fulfill your learning target, objective, or outcome
       for the task?)
    3. Review the list of upcoming tasks for the next planned pomodoro time
       blocks, and start reflecting on or updating them.

    Cirillo suggests: Specific cases should be handled with common sense:
    If you finish a task while the Pomodoro is still ticking, the following
    rule applies: If a Pomodoro begins, it has to ring. It’s a good idea to
    take advantage of the opportunity for overlearning, using the remaining
    portion of the Pomodoro to review or repeat what you’ve done, make
    small improvements, and note what you’ve learned until the Pomodoro
    rings.
    """

    class State(enum.Enum):
        POMODORO = enum.auto()
        PAUSE = enum.auto()

    defaults = [
        ("work_interval_min", 25, "duration of one pomodoro in min"),
        ("pause_interval_min", 5, "duration of a pause in min"),
        (
            "pause_autostart_delay",
            1.0,
            "delay to start pause timer after pomtimer finished (0 disables",
        ),
        ("change_time_step", 0.5, "modify pomodoro timer in steps"),
        ("icon_resetted", "🍅", "text char/icon for pomodoro timer, state: pomodoro"),
        (
            "icon_resetpause",
            "💤",
            "text char/icon for pomodoro timer, state: running pause",
        ),
        (
            "icon_running",
            "🏃",
            "text char/icon for pomodoro timer, state: running a pomodoro",
        ),
        ("icon_paused", "⌛", "text char/icon for pomodoro timer, state: paused timer"),
        (
            "icon_finished",
            "✅",
            "text char/icon for pomodoro timer, state: finished pomodoro/pause",
        ),
        ("action_warning", False, "Not implemented..."),
        (
            "action_finished",
            "group:HAVE_A_BREAK",
            "Action to execute when pom finished. Either: False, 'lock', 'group:<NAME>'",
        ),
        (
            "action_lock_cmd",
            "/usr/bin/loginctl lock-session",
            "run command as lock action",
        ),
        (
            "action_unlock_cmd",
            "/usr/bin/loginctl unlock-session",
            "run command as unlock action",
        ),
        ("notification_title", "PomodoroTimer", ""),
    ]

    def __init__(self, **config):
        super().__init__(
            pomname="PomodoroTimer",
            **config,
        )
        self.add_defaults(PomodoroTimer.defaults)
        self.pom_setup_icons()
        # setup lock/unlock commands
        self.action_lock_cmd = shlex.split(self.action_lock_cmd)
        self.action_unlock_cmd = shlex.split(self.action_unlock_cmd)

        # init main pomtimer
        self._pom_state = PomodoroTimer.State.POMODORO
        self.pomtimer = Timer(name="Pomodoro")
        self.pomtimer.duration = self.work_interval_min * 60.0
        self.pomtimer.add_notification_callback(
            func=self.pom_warning,
            fire_at_quota=0.9,
            name="warning callback",
        )
        self.pomtimer.add_notification_callback(
            func=self.pom_finished,
            name="finished callback",
        )
        self._loop = asyncio.get_running_loop()

    def pom_setup_icons(self):
        self._pom_icons = {
            Timer.State.RESETTED: self.icon_resetted,
            Timer.State.RUNNING: self.icon_running,
            Timer.State.PAUSED: self.icon_paused,
            Timer.State.FINISHED: self.icon_finished,
        }

    # implement buttons callback:
    def _button1(self):
        self.startstop()

    def _button3(self):
        self.switch_state()

    def _button4(self):
        """scroll up"""
        self.update_duration(self.change_time_step)

    def _button5(self):
        """scroll down"""
        self.update_duration(-1 * self.change_time_step)

    def render_output(self) -> str:
        p = "{picon} {ptime}".format(
            picon=self.pom_get_icon(),
            ptime=self.get_remaining_time(),
        )
        return p

    def update(self):
        super().update()
        if self.pomtimer.is_running():
            self._timerhandle = self._loop.call_later(delay=1.0, callback=self.update)

    # POMODORO interface
    @expose_command()
    def startstop(self):
        if self._pom_state == PomodoroTimer.State.PAUSE and self.pomtimer.is_finished():
            self.switch_state()
        self.pomtimer.startstop()
        self.log.info(
            "Pomodoro-%s %s. remaining: %s",
            "Timer" if self._pom_state == PomodoroTimer.State.POMODORO else "Pause",
            "started" if self.pomtimer.is_running() else "stopped",
            convert_seconds_to_ui_duration(self.pomtimer.remaining),
        )
        for c, nbc in enumerate(self.pomtimer.notification_callbacks):
            self.log.debug("timer:%d %s", c, str(nbc))
        self.update()

    @expose_command()
    def switch_state(self):
        if self._pom_state == PomodoroTimer.State.POMODORO:
            if self.pomtimer.reset():
                self.pomtimer.duration = self.pause_interval_min * 60
                self._pom_icons[Timer.State.RESETTED] = self.icon_resetpause
                self._pom_icons[Timer.State.RUNNING] = self.icon_resetpause
                self._pom_state = PomodoroTimer.State.PAUSE
                self.log.info("switched from Timer to Pause")
        else:
            if self.pomtimer.reset():
                self.pomtimer.duration = self.work_interval_min * 60
                self._pom_icons[Timer.State.RESETTED] = self.icon_resetted
                self._pom_icons[Timer.State.RUNNING] = self.icon_running
                self._pom_state = PomodoroTimer.State.POMODORO
                self.log.info("switched from Pause to Timer")
        self.update()

    @expose_command()
    def update_duration(self, step: float):
        newdur = self.pomtimer.duration + 60 * step
        self.reset_pomtimer(newdur)

    @expose_command()
    def reset_pomtimer(self, duration: float):
        if self.pomtimer.reset():
            self.pomtimer.duration = duration
        if self._pom_state == PomodoroTimer.State.POMODORO:
            self.work_interval_min = self.pomtimer.duration / 60
        else:
            self.pause_interval_min = self.pomtimer.duration / 60
        self.update()

    @expose_command()
    def get_remaining_time(self):
        try:
            when = self.pomtimer.remaining
            ret = f"{int(when / 60):02d}:{int(when % 60):02d}"
        except Exception:
            ret = asyncio.get_running_loop().time()
        return ret

    def pom_get_icon(self):
        try:
            ricon = self._pom_icons[self.pomtimer.state]
        except KeyError:
            ricon = "X"
        return ricon

    def pom_finished(self):
        self.log.info(
            "Pomodoro-%s finished.",
            "Timer" if self._pom_state == PomodoroTimer.State.POMODORO else "Pause",
        )
        if self._pom_state == PomodoroTimer.State.POMODORO:
            self._send_notification("Finished!", urgent=True)
        if self.action_finished:
            self.pom_action()

    def pom_action(self):
        if self._pom_state == PomodoroTimer.State.POMODORO:
            # take only action after pom (not after pause)
            match self.action_finished.split(":"):
                case ["lock"]:
                    subprocess.run(self.action_lock_cmd)
                case ["group", pomgrp]:
                    self._workgrp = self.qtile.current_group
                    try:
                        self.qtile.groups_map[pomgrp].toscreen()
                    except KeyError:
                        self.qtile.add_group(pomgrp)
                        self.qtile.groups_map[pomgrp].toscreen()
            if float(self.pause_autostart_delay) > 0:
                d = max((1.0, float(self.pause_autostart_delay)))
                self._loop.call_later(d, self.switch_state)
                self._loop.call_later(d, self.startstop)
        elif self._pom_state == PomodoroTimer.State.PAUSE:
            match self.action_finished.split(":"):
                case ["lock"]:
                    subprocess.run(self.action_unlock_cmd)
                case ["group", pomgrp]:
                    try:
                        if self.qtile.current_group == self.qtile.groups_map[pomgrp]:
                            # only jump back if we are still at the break group
                            self._workgrp.toscreen()
                    except KeyError:
                        # if workgroup was not set before, just ignore
                        pass

    def pom_warning(self):
        if self._pom_state == PomodoroTimer.State.POMODORO:
            self._send_notification("Ending soon ...")
        # TODO warning actions?


class AlarmsMenu(Popup):
    def __init__(self, qtile, alarms_cb, **config):
        super().__init__(qtile, **config)
        self._alarms_cb = alarms_cb
        self._loop = asyncio.get_running_loop()
        self._visible = False

    @property
    def visible(self):
        return self._visible

    @visible.setter
    def visible(self, onoff):
        # only act on switching states:
        if self.visible and not onoff:
            self._timerhandle.cancel()
            self._visible = False
        elif not self.visible and onoff:
            self._visible = True
            self.update()

    def unhide(self):
        super().unhide()
        self.visible = True
        self.place()

    def hide(self):
        super().hide()
        self.visible = False

    def process_button_click(self, x, y, button) -> None:
        if button == 3:
            self.hide()

    def update(self) -> None:
        if self.visible:
            self.text = self.render_text()
            self.clear()
            self.draw_text()
            self.draw()
            self._timerhandle = self._loop.call_later(delay=1.0, callback=self.update)

    def render_text(self):
        lines = [
            '<span underline="single">Alarms</span>:',
        ]
        for idx, alarm in enumerate(self._alarms_cb()):
            if alarm.is_running():
                icon = self.icon_run
                attime = alarm.localtime.strftime("%H:%M:%S")
            else:
                icon = self.icon_paused
                attime = "--:--:--"

            lines.append(
                "{idx} {icon} {remain:8s} at {attime} {name}".format(
                    idx=idx,
                    icon=icon,
                    name=alarm.name,
                    remain=convert_seconds_to_ui_duration(alarm.remaining),
                    attime=attime,
                )
            )
        return "\n".join(lines)


class PomodoroAlarms(PomodoroBase):
    """# Pomodoro Alarms

    A nice company to Pomodoro Timer are alarms. These use the same timer
    implementation and action management.
    """

    class State(enum.Enum):
        NOALARMS = enum.auto()
        ALARMS = enum.auto()

    defaults = [
        ("prompt", "⏰", "prompt for input"),
        ("icon_alarms", "⏰", "text char/icon for alarms"),
        ("icon_noalarms", "🔕", "text char/icon for alarms"),
        ("popup_border", "#ff0000", "color of border"),
        ("popup_border_width", 0, "Line width of drawn borders."),
        ("popup_font", "Noto Sans Mono", "Font used in notifications."),
        ("popup_icon_run", "▶", "Size of font."),
        ("popup_icon_paused", "⏸", "Size of font."),
        ("popup_font_size", 12, "Size of font."),
        ("notification_title", "PomodoroAlarms", ""),
    ]

    def __init__(self, **config):
        super().__init__(**config)
        self._config = config
        self.add_defaults(PomodoroBase.defaults)
        self.add_defaults(PomodoroAlarms.defaults)
        self.setup_icons()
        self._alarms = list()
        self.state = PomodoroAlarms.State.NOALARMS
        self.menu = None
        self._close_after_parse = False
        self._loop = asyncio.get_running_loop()

    def timer_setup(self):
        super().timer_setup()
        self.menu = AlarmsMenu(
            qtile=self.qtile,
            alarms_cb=self.alarm_list,
            **{
                k.removeprefix("popup_"): v
                for k, v, doc in PomodoroAlarms.defaults
                if k.startswith("popup_")
            },
        )
        try:
            self.promptwidget = self.qtile.widgets_map["prompt"]
            self.prompt_completer = TimerCompleter(self.qtile)
        except KeyError:
            logger.warning("PomodoroAlarms needs the Prompt widget.")

    def _get_next_timer(self):
        timer = {timer.remaining: timer for timer in self.alarms}
        next_timer = timer[min(timer.keys())]
        return next_timer

    def render_output(self) -> str:
        match self.state:
            case PomodoroAlarms.State.NOALARMS:
                ret = self.icon
            case PomodoroAlarms.State.ALARMS:
                nr_timer = len(self.alarms)
                next_timer = self._get_next_timer()
                remaining = next_timer.remaining
                remain = convert_seconds_to_ui_duration(remaining)
                if remaining < 3600:
                    # if less than an on hour cut to mm:ss
                    remain_time = remain[-5:]
                else:
                    # else cut to hh:mm
                    remain_time = remain[:5]
                ret = "{icn}{more:s} {remain:5s} {name}".format(
                    icn=self.icon,
                    remain=remain_time,
                    name=next_timer.name[:10],
                    more=f"{nr_timer - 1:+d}" if nr_timer > 1 else "",
                )
            case _:
                logger.warning("Alarms widget has bad state. Cannot render")

        return ret

    def update(self):
        super().update()
        if self.menu:
            self.menu.update()
        if self.state == PomodoroAlarms.State.ALARMS:
            self._loop.call_later(1.0, self.update)

    # ALARMS imterface
    @property
    def state(self):
        return (
            PomodoroAlarms.State.ALARMS
            if len(self.alarms) > 0
            else PomodoroAlarms.State.NOALARMS
        )

    @state.setter
    def state(self, newstate):
        if newstate in PomodoroAlarms.State:
            self._state = newstate

    @property
    def alarms(self):
        return self._alarms

    @property
    def icon(self):
        icon = self._alarms_icons[self.state]
        return icon

    def _button1(self):
        self.alarm_prompt()

    def mouse_enter(self, x, y):
        self.menu.unhide()

    def mouse_leave(self, x, y):
        self._loop.call_later(1, self.menu.hide)

    def setup_icons(self):
        self._alarms_icons = {
            PomodoroAlarms.State.NOALARMS: self.icon_noalarms,
            PomodoroAlarms.State.ALARMS: self.icon_alarms,
        }

    def parse_user_input(self, userinput: str) -> None:
        try:
            match userinput.split():
                case ["in", time, *name]:
                    self._add_alarm(
                        duration=convert_ui_duration_to_seconds(time),
                        name=" ".join(name),
                    )
                case ["at", time, *name]:
                    self._add_alarm(
                        duration=convert_ui_attime_to_duration(time),
                        name=" ".join(name),
                    )
                case ["res", index]:
                    timer = self.alarms[int(index)]
                    timer.stop()
                    timer.reset()
                case ["set", index, "in", time]:
                    timer = self.alarms[int(index)]
                    timer.stop()
                    timer.duration = convert_ui_duration_to_seconds(time)
                    timer.startstop()
                case ["set", index, "at", time]:
                    timer = self.alarms[int(index)]
                    timer.stop()
                    timer.duration = convert_ui_attime_to_duration(time)
                    timer.startstop()
                case ["rm", index]:
                    self.alarm_rm(int(index))
                case ["ss", index]:
                    self.alarm_ss(int(index))
                case ["mv", index, *newname]:
                    timer = self.alarms[int(index)]
                    timer.name = " ".join(newname)
            self.update()
        except Exception as e:
            logger.error(e)
            self._send_notification(message=str(e), urgent=False)

    def _add_alarm(
        self,
        duration: float,
        name: str,
        start_immediately: bool = True,
    ) -> None:
        try:
            newalarm = Timer(name, duration)
            newalarm.add_notification_callback(
                partial(self.alarm_finished, newalarm),
                name=name,
            )
            if start_immediately:
                newalarm.startstop()
            self._alarms.append(newalarm)
        except Exception as e:
            self._send_notification(message=str(e), urgent=False)

    # qtile interface
    @expose_command()
    def parse_input(self, inputstr: str) -> None:
        self.parse_user_input(inputstr)
        if self._close_after_parse:
            self._loop.call_later(1, self.menu.hide)

    @expose_command()
    def alarm_prompt(self) -> None:
        self._close_after_parse = not self.menu.visible
        if self._close_after_parse:
            self.menu.unhide()
        self.promptwidget.start_input(
            prompt=self.prompt,
            callback=self.parse_input,
            allow_empty_input=True,  # so close callback is run also when empty input
        )
        self.promptwidget.completer = self.prompt_completer

    @expose_command()
    def alarm_rm(self, index: int) -> None:
        try:
            self.alarms[index].stop()
            del self.alarms[index]
        except IndexError:
            logger.warning(f"Alarm {index} does not exist. Cannot remove it")

    @expose_command()
    def alarm_ss(self, index: int) -> None:
        try:
            self.alarms[index].startstop()
        except IndexError:
            logger.warning(f"Alarm {index} does not exist. Cannot startstop it")

    def alarm_finished(self, alarm) -> None:
        self._send_notification(message=alarm.name, urgent=True)
        alarm.stop()  # double tap
        self._alarms.remove(alarm)
        # need update here, bc this is a callback
        self.update()

    @expose_command()
    def alarm_list(self):
        return self.alarms


class TimerCompleter(AbstractCompleter):
    """TODO"""

    def __init__(self, qtile) -> None:
        self.qtile = qtile

    def actual(self) -> str:
        return ""

    def reset(self) -> None:
        pass

    def complete(self, txt: str, aliases: dict[str, str] | None = None) -> str:
        return txt + "_"


class Timer:
    """
    # The countdown timer

    Using asyncio main loop to schedule callbacks

    """

    MAX_DURATION = 60 * 60 * 24 * 7
    MIN_DURATION = 0

    class State(enum.Enum):
        UNDEF = enum.auto()
        RESETTED = enum.auto()
        RUNNING = enum.auto()
        PAUSED = enum.auto()
        FINISHED = enum.auto()

    class NotificationCallback:
        def __init__(
            self,
            func: Callable,
            fire_at_quota: float = 1.0,
            name: str | None = None,
        ):
            self._cb = func
            self._set_quota(fire_at_quota)
            self._name = name
            self._loop = asyncio.get_running_loop()
            self._timerhandle: asyncio.TimerHandle | None = None

        def _set_quota(self, fire_at_quota):
            # allowed range = [0..1]
            allowed_range = [0.0, float(fire_at_quota), 1.0]
            allowed_range.sort()
            self._fire_at_quota = allowed_range[1]

        def arm(self, begin: float, end: float, duration: float):
            now = self._loop.time()
            paused_time = end - begin - duration
            fire_at_time = begin + self._fire_at_quota * duration + paused_time
            fire_in_secs = fire_at_time - now
            if fire_in_secs > 0:
                logger.debug(f"arming alarm in {fire_in_secs}s")
                self._timerhandle = self._loop.call_later(
                    delay=fire_in_secs,
                    callback=self._cb,
                )
            else:
                logger.debug(f"not arming alarm in {fire_in_secs}s")

        def disarm(self):
            try:
                self._timerhandle.cancel()
                del self._timerhandle
                self._timerhandle = None
            except Exception:
                logger.debug("exception during disarming timer")

        @property
        def when(self) -> float:
            ret = -1.0
            if self._timerhandle:
                ret = (
                    self._timerhandle.when()
                    if not self._timerhandle.cancelled()
                    else -1.0
                )
            return ret

        def __str__(self):
            insec = self.when - self._loop.time()
            if insec > 0:
                ret = "name:{name}, fire in {when_sec:.2f}s {fq:.2f}%".format(
                    name=self._name,
                    when_sec=self.when - self._loop.time(),
                    fq=self._fire_at_quota * 100,
                )
            else:
                ret = "name:{name}, not armed".format(
                    name=self._name,
                )
            return ret

    def __init__(
        self,
        name: str = "Timer w/o name",
        duration: float = 600,
    ):
        self._name = name
        self._notification_callbacks: list[Timer.NotificationCallback] = list()

        self._state = Timer.State.UNDEF
        self.add_notification_callback(self._finished, name=f"finished timer of {name}")
        self.duration = duration

        # get the loop and schedule a call at the end:
        self._loop = asyncio.get_running_loop()

    def add_notification_callback(
        self,
        func: Callable,
        fire_at_quota: float = 1.0,
        name: str = "callback w/o name",
    ):
        ncb = Timer.NotificationCallback(
            func, fire_at_quota, name=f"{self.name}::{name}"
        )
        self._notification_callbacks.append(ncb)
        return ncb

    @property
    def notification_callbacks(self):
        return self._notification_callbacks

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, newname: str) -> None:
        self._name = newname

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, newstate):
        self._state = newstate

    def is_running(self) -> bool:
        return self.state == Timer.State.RUNNING

    def is_paused(self) -> bool:
        return self.state == Timer.State.PAUSED

    def is_resetted(self) -> bool:
        return self.state == Timer.State.RESETTED

    def is_finished(self) -> bool:
        return self.state == Timer.State.FINISHED

    @property
    def now(self):
        return self._loop.time()

    @property
    def start_time(self):
        return self._start_time

    @start_time.setter
    def start_time(self, time: float):
        self._start_time = time

    @property
    def paused_at_time(self):
        return self._paused_at_time

    @paused_at_time.setter
    def paused_at_time(self, time: float):
        self._paused_at_time = time

    @property
    def end_time(self):
        return self._end_time

    @end_time.setter
    def end_time(self, time: float):
        self._end_time = time

    @property
    def duration(self) -> float:
        return self._duration

    @duration.setter
    def duration(self, time_secs: float) -> None:
        if not self.is_running():
            self._duration = time_secs
            if self._duration < Timer.MIN_DURATION:
                self._duration = Timer.MIN_DURATION
            elif self._duration > Timer.MAX_DURATION:
                self._duration = Timer.MAX_DURATION
            self.reset()

    @property
    def remaining(self) -> float:
        if self.is_running():
            rem = self.end_time - self.now
        elif self.is_paused():
            # rem = (self.end_time - self.now) + (self.now - self.paused_at_time)
            rem = self.end_time - self.paused_at_time
        elif self.is_resetted():
            rem = self.duration
        elif self.is_finished():
            rem = 0.0
        return rem

    @property
    def localtime(self) -> datetime | None:
        """localtime when the timer finishes"""
        if self.is_running():
            now = datetime.now()
            delta = timedelta(seconds=self.end_time - self.now)
            return now + delta
        else:
            return None

    def startstop(self):
        """this method handles the transition between the states"""
        if self.is_running():
            self._pause()
        elif self.is_paused():
            # account for the paused time, increase end time
            # and start the timer again
            self.end_time += self.now - self.paused_at_time
            self._start()
        elif self.is_resetted():
            # run new timer from now on, set end at duration
            self.begin_time = self.now
            self.end_time = self.begin_time + self.duration
            self._start()

    def stop(self):
        """Set the timer to pause if running.
        Also cancle all scheduled callbacks, which happens when pausing
        """
        if self.is_running():
            self._pause()

    def reset(self):
        """reset the timer to the configured values"""
        resetted = False
        if not self.is_running():
            self.state = Timer.State.RESETTED
            self.begin_time = 0.0
            self.end_time = 0.0
            resetted = True
        return resetted

    def _arm_callbacks(self):
        for cb in self._notification_callbacks:
            cb.arm(self.begin_time, self.end_time, self.duration)

    def _disarm_callbacks(self):
        for cb in self._notification_callbacks:
            cb.disarm()

    def _pause(self):
        self.state = Timer.State.PAUSED
        self.paused_at_time = self.now
        self._disarm_callbacks()

    def _start(self):
        self._arm_callbacks()
        self.state = Timer.State.RUNNING

    def _finished(self):
        self.state = Timer.State.FINISHED
