# PomodoroTimer And Alarms

This widget is suited to be integrated into the [Qtile](http://www.qtile.org)
widget bar. It provides two functions:

1. The [Pomodoro Method](https://en.wikipedia.org/wiki/Pomodoro_Technique)
  - _tl;dr;_ improve your work-life balance, turn away from your screen at regular
    intervals. pomodoro helps you doing this
      - define a task (a task could also be, defining tasks for the day)
      - (start the pomodoro timer, have a short break) * 3 times
      - have a long break

2. Simple alarm timers with reminder

## Usage


The widget responds to mouse events and can be managed through qtile commands.

### Pomodoro Timer
The Pomodoro Timer works with two timers (two states `working` and `pausing`).
Which timer is currently active and if it is running or paused, is indicated by
the icon.


#### 1.  Working-timer: ![pomwidget reset](./img/scrot_reset.jpg)

On startup the pomodoro timer is reset to state working. From there it can be
  - started with left mouse button (`cmd_startstop()`)
  - or the duration can be changed with scroll up/down (
    `cmd_update_duration()`). The duration is stored for the next reset.
  - or switch between working/pausing timer with the right mouse button
    (`cmd_switch_state()`)

#### 2. Running working-timer: ![pomwidget running](./img/scrot_pom_running.jpg)

A running timer can be halted and continued with left mouse button.
The duration of a halted timer can also be inc/decreased with the mouse wheel.

#### 3. Halted timer: ![pomwidget paused](./img/scrot_paused.jpg)

A halted or finished timer can be resetted with right mouse button.

#### 4. Finished timer: ![pomwidget finished](./img/scrot_finished.jpg)

The working-timer will execute two actions (see configuration):

1. warning action at 90% of the duration (default)
  - a notification will be send
2. execute an action when finished
  - `group:GRPNAME` (default): jump to the group $GRPNAME. This will leave
    your current working screen and jump to the/a new empty group. This is
    pomodoro urging you to stop watching at your screen.
  - `lock`: lock the screen

Currently, only one finished-action is supported.

If configured (default) the pausing-timer will be started automatically after
the working-timer is finished. So you do not have to manually start it.

#### 5. Running pausing-timer: ![pomwidget pause](./img/scrot_pause_running.jpg)

The pausing-timer does not have a warning action. The finish-actions are:

1. send a notification
2. revert the action taken by the finished working-timer
  - `group:GRPNAME`: let qtile jump back to your working screen
  - `lock`: unlock your screen

A single left-button click on the finished pausing-timer
resets it to the work-timer and starts it.


### PomodoroAlarms

As the Timer class was working just fine, I wanted to also run multiple
arbitrary timers which send me notifications or do other things. In the end, the
implementation was much more complicated than the pomodoro timer. Anyways, here
you go:

#### 1. Alarms are ephemeral
After restart of qtile all timers are gone (could be changed, but I did not aim
for long standing and persistent alarms; there are calendars). As seen in the
screen shot above, the Alarms widget shows the next due alarm and a number of
further alarms in the list. If there are no alarm, this is indicated by the
icon.

#### 2. Mouse interaction
1. Mouse enters: The alarms popup shows
2. Left Click: The cli prompt activates
3. Mouse leaves: The alarms popup disappears after 1 second

#### 3. The alarm popup 

![almwidget](./img/scrot_alarms_popup.jpg)

```
list format::
    id, icon, countdown_time, finish_time, name

id: used in cmdline to modify alarms
icon: show the state, running or paused
```

#### 4. The alarm prompt 

The alarm prompt is a cmd line interface to the alarms. See configuration about
how you start the alarm prompt. The commands are:
```
    in|at <time> <name>    add new alarm
    ss <id> <name>         startstop alarm with id
    mv <id> <name>         rename
    res <id>               reset
    set <id> in|at <time>  set new time
    rm <id>                delete

    <id> int:      id of the alarm from the popup list
    <time> str     either used with at, which expects an absolute time or with in to give a duration (HH:MM:SS, at HH:MM, in MM:SS)
    <name> str     name of time (including spaces)
```

#### 5. Alarm States

The state of the alarms is indicated by the icons in the widget. Per default
it shows:
  - When no alarms are active, a strike-through alarm clock icon
  - When alarms are active, an alarm clock icon the countdown of the next
    alarm. Also the number of further alarms is shown, if any. To get the
    countdown and the localtime when the alarm will fire, see the Alarm popup.

## Logging

Per default the pomodoro timer widget writes information to the qtile logs. See
the configuration options about logging. It is possible to also write timer
related logs to another file, if you want to track the times you have worked
with pomodoro.

## Installation

QTile comes with a decent number of widets included.
At the time of writing (2022-07-28), PomodoroTimer is not part of the upstream
QTile software package. I intend to post a PR to their repo as soon as I think
the PomodoroTimer is mature enough to be added to the distribution.

So long, PomodoroTimer and PomodoroAlarms can be added manually in the configuration as follows:

1. Copy the file `pomodoro_btwe.py` next to `config.py`; the configuration file
   of QTile. The default location on my linux is `~/.config/qtile/pomodoro_btwe.py`.
2. In `config.py`
  - Add the import:
  ```
  from pomodoro_btwe import PomodoroTimer, PomodoroAlarms
  ```
  - Add the widget to the bar(s). e.g.:
  ```
  pom_widget = PomodoroTimer() # create a single instance, to use the
                               # mirror-function, if used on two screens
  alarm_widget = PomodoroAlarms()
  screens = [
    Screen(
        top=bar.Bar(
            [
                ...,
                widget.GroupBox(...),
                widget.Sep(),
                pom_widget,
                widget.Sep(),
                alarm_widget,
                ...,
            ]
    ),
   ```
   - Add a keybind to start the alarm prompt:
   ```
   def timerprompt(qtile):
       qtile.widgets_map['pomodoroalarms'].cmd_alarm_prompt()
   ...
   Key([mod], "semicolon", lazy.function(timerprompt)),
   ```

## Dependencies

- python >= 3.10
- python-dbus-next
- noto fonts

My reference and development system is archlinux. The current python version
here is `3.10.5`. I made use of the new [match statement](https://docs.python.org/3/reference/compound_stmts.html#the-match-statement)
which was introduced in python v3.10. Let me know if you would desperately need
backwards compatibility. Also, the default installation of qtile on archlinux
specifies an optional dependency which is needed to send notifications via the
dbus interface. Make sure your python provides the package `python-dbus-next`.

Furthermore, PomodoroTimer and - Alarms render strings and uses unicode icons to
indicate the state of the timers. I have installed the packages `noto-fonts`
and `noto-fonts-emojis` which show them nicely. Alternatively, if you don't
like the default icons, change the configuration to use other characters for
the icons.

## Configuration
TBD .. so long look into 
  - `PomodoroBase.defaults`
  - `PomodoroTimer.defaults`
  - `PromodoroAlarms.defaults`
